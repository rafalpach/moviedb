import React from 'react';
import axios from 'axios';
import CircularProgress from 'material-ui/CircularProgress';
import Snackbar from 'material-ui/Snackbar';
import InputSearch from '../Input/InputSearch.js';
import ButtonSearch from '../Button/ButtonSearch.js';
import DataTable from '../Table/DataTable.js';
import { GET_MOVIE } from '../Endpoint/endpoints.js';
import '../../styles/App/AppContainer.css';

let tmpTitle;

class AppContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      data: [],
      results: [],
      progress: false,
      open: false
    }
  }

  setInputValue(value) {
    this.setState({
      inputValue: value,
      open: false
    });
  }

  getMovieData(page) {
    const checkTitle = (value, tmp) => tmp !== undefined && value.length === 0 ? tmp : value;
    if (page === 1) this.setState({ progress: true });

    axios.get(GET_MOVIE + `&query=${checkTitle(this.state.inputValue, tmpTitle)}` + `&page=${page}`)
      .then(response => {
        if (page > 1) {
          this.setState({
            data: response.data,
            results: this.state.results.concat(response.data.results),
            progress: false
          });
        } else {
          this.setState({
            data: response.data,
            results: response.data.results,
            progress: false
          });
        }
      })
      .catch(reject => {
        this.setState({
          progress: false,
          open: true
        });
      })

    if (page === 1) tmpTitle = this.state.inputValue; 
    this.setInputValue('');
  }

  render() {
    return (
      <div>
        <h1 className="title">MOVIE DB</h1>
        <div className="search-bar">
          <InputSearch
            params={this.state.inputValue}
            onInputChange={value => this.setInputValue(value)} 
          />
          <ButtonSearch 
            params={this.state.inputValue}
            clickSearch={page => this.getMovieData(page)}
          />
        </div>
        {
          this.state.progress === true &&
          <div className="progress">
            <CircularProgress size={60} thickness={7} />
          </div>
        }
        {
          this.state.data.length !== 0 && this.state.progress === false &&
          <DataTable 
            showMoreMovies={page => this.getMovieData(page)}
            data={this.state.data} 
            results={this.state.results}
          />
        }
        <Snackbar
          open={this.state.open}
          message="Oops! We encountered a problem :("
          autoHideDuration={4000}
        />
      </div>
    )
  }
}

export default AppContainer;