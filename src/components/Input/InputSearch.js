import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';

const stylesObj = {
  marginRight: '50px'
}

class InputSearch extends React.Component {
  handleChange(event) {
    this.props.onInputChange(event.target.value);
  }

  render() {
    return (
      <TextField
        style={stylesObj}
        value={this.props.params}
        onChange={event => this.handleChange(event)}
        hintText="Enter Movie Title" 
        id="input-search" 
      />
    );
  }
}

export default InputSearch;

InputSearch.propTypes = {
  params: PropTypes.string,
  onInputChange: PropTypes.func
};