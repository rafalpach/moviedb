import React from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';
import SearchIcon from 'material-ui/svg-icons/action/search';

class ButtonSearch extends React.Component {
  handleClick(num) {
    this.props.clickSearch(num);
  }

  render() {
    return <RaisedButton
      disabled={this.props.params.length === 0}
      onClick={() => this.handleClick(1)}
      icon={<SearchIcon color="black" />} 
      label="Search" 
    />
  }
}

export default ButtonSearch;

ButtonSearch.propTypes = {
  params: PropTypes.string,
  clickSearch: PropTypes.func
};