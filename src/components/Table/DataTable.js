import React from 'react';
import PropTypes from 'prop-types';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import { GET_IMG} from '../Endpoint/endpoints.js';
import '../../styles/Table/DataTable.css';

const stylesObj = {
  table: {
    whiteSpace: 'normal'
  },
  button: {
    textAlign: 'center'
  }
}

class DataTable extends React.Component {
  cutText(text) {
    return text.length > 150 ? `${text.slice(0, 150)}...` : text;
  }

  handleClick(num) {
    this.props.showMoreMovies(num);
  }

  render() {
    if (this.props.data !== undefined && this.props.data.total_results === 0) {
      return <p className="paragraph">NO RESULTS FOUND</p>;
    } else {
      return (
        <div>
          <Table>
            <TableHeader>
              <TableRow>
              <TableHeaderColumn>Poster</TableHeaderColumn>
              <TableHeaderColumn>Title</TableHeaderColumn>
              <TableHeaderColumn>Release Date</TableHeaderColumn>
              <TableHeaderColumn>Score</TableHeaderColumn>
              <TableHeaderColumn>Count</TableHeaderColumn>
              <TableHeaderColumn>Overview</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody>
              {
                this.props.results.map((movie) => {
                  return (
                    <TableRow key={movie.id}>
                      <TableRowColumn>
                        {
                          movie.poster_path === null
                          ? <div className="placeholder"></div>
                          : <img width="100px" heigth="150px" src={GET_IMG + movie.poster_path} />
                        }
                      </TableRowColumn>
                      <TableRowColumn>{movie.original_title}</TableRowColumn>
                      <TableRowColumn>{movie.release_date}</TableRowColumn>
                      <TableRowColumn>{movie.vote_average}</TableRowColumn>
                      <TableRowColumn>{movie.vote_count}</TableRowColumn>
                      <TableRowColumn style={stylesObj.table}>{this.cutText(movie.overview)}</TableRowColumn>
                    </TableRow>
                  )
                })
              }
            </TableBody>
          </Table>
          {
            this.props.data.total_pages > 1 && this.props.data.total_pages !== this.props.data.page &&
            <div className="btn-more">
              <RaisedButton
                onClick={() => this.handleClick(this.props.data.page + 1)}
                label="Show more..."
              />
            </div>
          }
        </div>
      )
    }
  }
}
  
export default DataTable;

DataTable.propTypes = {
  data: PropTypes.object,
  results: PropTypes.array,
  showMoreMovies: PropTypes.func
};