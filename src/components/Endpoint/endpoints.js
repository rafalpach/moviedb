import apiKey from './apiKey.js';

const GET_MOVIE = `https://api.themoviedb.org/3/search/movie?${apiKey}&language=en-US`;
const GET_IMG = 'https://image.tmdb.org/t/p/w500/';

export { GET_MOVIE, GET_IMG };