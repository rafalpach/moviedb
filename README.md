Movie DB Project

Setup
Install dependencies
$ npm install

Development
Run local server on http://localhost:8080/
$ npm run dev

Deployment
Build the current application
$ npm run build